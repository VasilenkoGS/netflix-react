import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import { BrowserRouter as Router } from 'react-router-dom';
import NetflixApp from './components/NetflixApp/NetflixApp';
import registerServiceWorker from './registerServiceWorker';

ReactDOM.render((
    <Router>
        <NetflixApp/>
    </Router>),
    document.getElementById('root'));


registerServiceWorker();