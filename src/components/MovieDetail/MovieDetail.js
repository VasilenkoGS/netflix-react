import React    from "react";
import MovieDetailJSX from "./MovieDetail.jsx";

class MovieDetail extends React.Component {
  render() {
    return MovieDetailJSX.call(this.props);
  }
}

export default MovieDetailJSX;
