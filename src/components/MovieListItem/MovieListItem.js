import React    from "react";
import template from "./MovieListItem.jsx";

class MovieListItem extends React.Component {
  constructor(props){
    super(props);
    this.state= {data: ["Andrey", "Ivan", "Olga"]}
  }
  render() {
    return template.call(this);
  }
}

export default MovieListItem;
