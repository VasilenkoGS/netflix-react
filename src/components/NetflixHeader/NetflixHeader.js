import React    from "react";
import template from "./NetflixHeader.jsx";

class NetflixHeader extends React.Component {
  render() {
    return template.call(this);
  }
}

export default NetflixHeader;
