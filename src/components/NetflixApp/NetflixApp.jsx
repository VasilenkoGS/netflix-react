import "./NetflixApp.css";
import React from "react";
import NetflixHeader from "../NetflixHeader";
import NetflixMain from "../NetflixMain";

function template() {
  return (
    <div className="netflix-app">
      <NetflixHeader></NetflixHeader>
      <NetflixMain></NetflixMain>
    </div>
  );
};

export default template;
