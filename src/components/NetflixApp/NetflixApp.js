import React    from "react";
import template from "./NetflixApp.jsx";

class NetflixApp extends React.Component {
  render() {
    return template.call(this);
  }
}

export default NetflixApp;
