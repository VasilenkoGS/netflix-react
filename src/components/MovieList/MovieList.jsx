import "./MovieList.css";
import React from "react";
import MovieDetail from "../MovieDetail";

function template(res) {
  return (
    <div className="movie-list">
      <MovieDetail/>
      <MovieDetail/>
      <MovieDetail/>
      <MovieDetail/>
      <MovieDetail/>
      <MovieDetail/>
      <MovieDetail/>
      <MovieDetail/>
      <MovieDetail/>
      <MovieDetail/>
      <MovieDetail/>
      <MovieDetail/>
      <MovieDetail/>
    </div>
  );
};

export default template;
