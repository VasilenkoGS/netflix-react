import "./NetflixMain.css";
import { Switch, Route, Redirect } from 'react-router-dom'
import React from "react";
import MovieList from "../MovieList";
import MovieListItem from "../MovieListItem";
import NetflixHeader from "../NetflixHeader";

function template() {
  return (
    <div className="netflix-main">
      <Switch>
        <Route exact path='/movies' component = { MovieList }/>
        <Route path='/movies/:id' component = { MovieListItem }/>
        <Redirect from="*" to="/movies"/>
      </Switch>
    </div>
  );
};

export default template;
