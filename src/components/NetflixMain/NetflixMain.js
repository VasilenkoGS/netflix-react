import React    from "react";
import template from "./NetflixMain.jsx";

class NetflixMain extends React.Component {
  render() {
    return template.call(this);
  }
}

export default NetflixMain;
